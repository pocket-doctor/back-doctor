# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-30 14:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('account', '0005_auto_20161227_1223'),
    ]

    operations = [
        migrations.CreateModel(
            name='DoctorMonthlyPromotion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date', models.DateField()),
                ('doctor', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='monthly_promotions', to='account.Doctor')),
            ],
        ),
        migrations.AlterUniqueTogether(
            name='doctormonthlypromotion',
            unique_together=set([('doctor', 'date')]),
        ),
    ]
