from django.conf import settings
from rest_framework.pagination import LimitOffsetPagination


class PromoteDoctorSetPagination(LimitOffsetPagination):
    default_limit = settings.PROMOTE_SEARCH_LIMIT

    def get_paginated_response(self, data):
        self.count = self.default_limit if self.count > self.default_limit else self.count
        return super(PromoteDoctorSetPagination, self).get_paginated_response(data)

    def get_limit(self, request):
        return self.default_limit

    def get_offset(self, request):
        return 0

    def get_next_link(self):
        return None

    def get_previous_link(self):
        return None
