from django.urls.base import reverse
from django.views.generic.edit import CreateView

from account.models import Doctor
from advertisement.forms.doctor_upgrade_forms import DoctorUpgradeForm


class DoctorUpgradeView(CreateView):
    model = Doctor
    template_name = 'advertisement/upgrade.html'
    form_class = DoctorUpgradeForm

    def get_form(self, form_class=None):
        form = super(DoctorUpgradeView, self).get_form(form_class)
        form.doctor = self.request.user.member.doctor
        return form

    def get_success_url(self):
        return reverse('home')
