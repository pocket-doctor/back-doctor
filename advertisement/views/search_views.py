import re
from datetime import date

from django.conf import settings
from django.db.models.query_utils import Q
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from account.models import Doctor
from account.serializers.doctor_serializers import DoctorSerializer
from advertisement.utils import PromoteDoctorSetPagination
from utils.base_rest_views import BaseRestViews


class PromoteDoctorViews(BaseRestViews):
    pagination_class = PromoteDoctorSetPagination
    serializer_class = DoctorSerializer
    lookup_fields = {
        'list': ('q',)
    }
    http_method_names = ['get']
    permission_classes = {
        'advertise': [IsAuthenticated],
        'list': [IsAuthenticated],
    }

    def __init__(self, **kwargs):
        self.query = None
        super(PromoteDoctorViews, self).__init__(**kwargs)

    def initial(self, request, *args, **kwargs):
        self.query = kwargs.get('q', '*')
        return super(PromoteDoctorViews, self).initial(request, *args, **kwargs)

    def get_queryset(self):
        month = date.today().replace(day=1)
        promote_doctors = Doctor.objects.filter(monthly_promotions__date=month)
        if self.query == '*':
            return promote_doctors
        db_filter = Q(member__user__first_name__icontains=self.query) | Q(
            member__user__last_name__icontains=self.query)
        for query in re.split('[\n `~!@#$%^&*()_\-+=\'";:?/>.<,]+', self.query):
            db_filter |= Q(member__user__first_name__icontains=query) | Q(
                member__user__last_name__icontains=query)
        return promote_doctors.filter(db_filter)

    def advertise(self, *args, **kwargs):
        month = date.today().replace(day=1)
        queryset = Doctor.objects.filter(monthly_promotions__date=month).order_by('?')
        serializer = self.get_serializer(queryset[:settings.ADVERTISE_LIMIT], many=True)
        return Response(serializer.data)
