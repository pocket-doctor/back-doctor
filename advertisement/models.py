from django.conf import settings
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import pre_save
from django.dispatch.dispatcher import receiver


class DoctorMonthlyPromotion(models.Model):
    doctor = models.ForeignKey('account.Doctor', related_name='monthly_promotions')
    date = models.DateField()

    class Meta:
        unique_together = ('doctor', 'date')


@receiver(pre_save, sender=DoctorMonthlyPromotion)
def check_limit_for_plan(instance, **kwargs):
    month = instance.date
    if DoctorMonthlyPromotion.objects.filter(
            date=month).count() >= settings.DOCTOR_PROMOTION_LIMIT_PER_MONTH:
        raise ValidationError("No more room for you, try another month")
