from rest_framework import status
from rest_framework.reverse import reverse

from advertisement.tests.base import AdvertisementBaseTest

__author__ = 'FERIne'


class ShowAdvertiseTest(AdvertisementBaseTest):
    def __init__(self, *args, **kwargs):
        super(ShowAdvertiseTest, self).__init__(*args, **kwargs)
        self.url = None

    def setUp(self):
        super(ShowAdvertiseTest, self).setUp()
        self.url = reverse('api:advertisement:show')

    def test_show_advertise(self):
        self.login(self.user1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
