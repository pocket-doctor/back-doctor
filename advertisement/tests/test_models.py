from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db.utils import IntegrityError

from account.models import Member, Doctor
from advertisement.tests.base import AdvertisementBaseTest

__author__ = 'FERIne'


class AdvertisementModelTest(AdvertisementBaseTest):
    def test_right_promotion(self):
        self.doctor.promote('2016-5-1')
        self.assertEqual(self.doctor.monthly_promotions.count(), 2)

    def test_twice_promotion_in_one_month(self):
        self.doctor.promote('2016-5-1')
        self.assertRaises(IntegrityError, self.doctor.promote, '2016-5-25')

    def test_promotion_limit_per_month(self):
        for i in range(0, settings.DOCTOR_PROMOTION_LIMIT_PER_MONTH):
            username = 'test' + str(i)
            email = username + '@test.com'
            melli_code = '12345' + str(100000 + i)
            phone = '+9891270' + str(100000 + i)
            user = User.objects.create_user(username, email, 'salam')
            member = Member.objects.create(user=user, melli_code=melli_code, phone_number=phone)
            doctor = Doctor.objects.create(member=member, clinic_phone=phone, clinic_address='x')
            doctor.promote('2016-5-1')
        self.assertRaises(ValidationError, self.doctor.promote, '2016-5-1')
