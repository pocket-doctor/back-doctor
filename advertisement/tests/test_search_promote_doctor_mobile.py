from rest_framework import status
from rest_framework.reverse import reverse

from advertisement.tests.base import AdvertisementBaseTest

__author__ = 'FERIne'


class PromoteDoctorSearchTest(AdvertisementBaseTest):
    def __init__(self, *args, **kwargs):
        super(PromoteDoctorSearchTest, self).__init__(*args, **kwargs)
        self.url = None
        self.success_data = None
        self.fail_data = None

    def setUp(self):
        super(PromoteDoctorSearchTest, self).setUp()
        self.url = reverse('api:advertisement:search', kwargs={'q': 'ham sal'})
        self.success_data = {
            'count': 1,
            'next': None,
            'previous': None,
            'results': [
                {
                    "id": 1,
                    "name": "Hamed Saleh",
                    "degree": None,
                    "degree_year": None,
                    "university": None,
                    "clinic_phone": "0213455432",
                    "clinic_address": "some where",
                    "schedule": 1
                }
            ]
        }
        self.fail_data = {
            'count': 0,
            'next': None,
            'previous': None,
            'results': []
        }

    def test_search(self):
        self.login(self.user1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.success_data)

    def test_search_all(self):
        self.url = reverse('api:advertisement:search', kwargs={'q': '*'})
        self.test_search()

    def test_search_fail(self):
        self.url = reverse('api:advertisement:search', kwargs={'q': 'chert'})
        self.login(self.user1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.fail_data)
