from datetime import date

from account.models import Doctor
from advertisement.models import DoctorMonthlyPromotion
from schedule.tests.base import ScheduleBaseTest

__author__ = 'FERIne'


class AdvertisementBaseTest(ScheduleBaseTest):
    def setUp(self):
        super(AdvertisementBaseTest, self).setUp()
        self.doctor = Doctor.objects.all()[0]
        DoctorMonthlyPromotion.objects.create(
            doctor=self.doctor, date=date.today().replace(day=1))
