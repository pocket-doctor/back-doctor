from django.conf.urls import url

from advertisement.views.doctor_upgrade_views import DoctorUpgradeView
from advertisement.views.search_views import PromoteDoctorViews

mobile_urlpatterns = [
    url(r'^search/(?P<q>[^w]+)/$', PromoteDoctorViews.as_view({'get': 'list'}), name='search'),
    url(r'^show/$', PromoteDoctorViews.as_view({'get': 'advertise'}), name='show'),
]

urlpatterns = [
    url(r'^upgrade/$', DoctorUpgradeView.as_view(), name='upgrade'),
]
