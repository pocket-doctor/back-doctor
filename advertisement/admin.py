from django.contrib import admin

from advertisement.models import DoctorMonthlyPromotion

admin.site.register(DoctorMonthlyPromotion)
