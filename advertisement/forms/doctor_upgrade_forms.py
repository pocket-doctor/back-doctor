import datetime

from django import forms
from django.forms.models import ModelForm

from account.models import Doctor


class DoctorUpgradeForm(ModelForm):
    year = forms.IntegerField(min_value=datetime.datetime.now().year)
    month = forms.IntegerField(max_value=12, min_value=1)

    class Meta:
        model = Doctor
        fields = ('year', 'month')

    def __init__(self, *args, **kwargs):
        super(DoctorUpgradeForm, self).__init__(*args, **kwargs)
        self.doctor = None

    def save(self, commit=True):
        data = self.cleaned_data
        self.doctor.promote(str(data['year']) + '-' + str(data['month']) + '-01')
        return self.doctor
