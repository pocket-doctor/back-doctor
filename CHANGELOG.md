# Change Log

This project adheres to [Semantic Versioning](http://semver.org/).  
Every release, along with the migration instructions, is documented on the Gitlab [Releases](https://gitlab.com/pocket-doctor/back-doctor/releases) page.
