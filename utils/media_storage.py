from django.conf import settings
from django.core.files.storage import default_storage
from gdstorage.storage import GoogleDriveStorage

if settings.STORAGE == 'default':
    DEFAULT_STORAGE = default_storage
else:
    DEFAULT_STORAGE = GoogleDriveStorage()


def get_file_url(url):
    if settings.STORAGE == 'default':
        return settings.SITE_URL + url
    else:
        file_id = url.split('/d/')[1].split('/')[0]
        return 'https://docs.google.com/uc?id=%s' % file_id
