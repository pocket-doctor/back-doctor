from django.utils.translation import ugettext_lazy as _
from rest_framework.exceptions import ValidationError

__author__ = 'FERIne'


class ConfirmPasswordValidator(object):
    message = _('Password and confirm password must be equal.')

    def __init__(self, password, confirm_password):
        self.instance = None
        self.password = password
        self.confirm_password = confirm_password

    def __call__(self, attrs):
        password = attrs.get(self.password, None)
        confirm_password = attrs.get(self.confirm_password, None)
        if not self.instance and password and (password != confirm_password):
            raise ValidationError(self.message)

    def set_context(self, serializer):
        self.instance = getattr(serializer, 'instance', None)
