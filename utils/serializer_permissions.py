from rest_framework import permissions

__author__ = 'FERIne'


class IsNotUser(permissions.IsAuthenticated):
    def has_permission(self, request, view):
        if request.method == 'OPTIONS':
            return True
        if not super(IsNotUser, self).has_permission(request, view):
            return True
        return False
