from rest_framework import viewsets, status
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import AllowAny
from rest_framework.response import Response


class BaseRestViews(viewsets.ModelViewSet):
    partial = True
    lookup_fields = {}
    lookup_fields_queryset = None
    permission_classes = {}

    def get_target_user(self):
        return None

    def get_permissions(self):
        if not self.action:
            return [AllowAny()]
        if type(self.permission_classes) == dict:
            return [permission() for permission in self.permission_classes[self.action]]
        return super(BaseRestViews, self).get_permissions()

    def get_object(self):
        if self.lookup_fields:
            query_filters = {}
            queryset = self.lookup_fields_queryset if self.lookup_fields_queryset else \
                self.get_queryset()
            fields = self.lookup_fields[self.action] if type(self.lookup_fields) == dict else \
                self.lookup_fields
            for field in fields:
                query_filters[field] = self.kwargs[field]
            obj = get_object_or_404(queryset, **query_filters)
            return obj
        return super(BaseRestViews, self).get_object()

    def initial(self, request, *args, **kwargs):
        self.kwargs['partial'] = kwargs['partial'] = self.partial
        return super(BaseRestViews, self).initial(request, *args, **kwargs)

    def create(self, request, request_data=None, *args, **kwargs):
        data = request_data if request_data else request.data
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
