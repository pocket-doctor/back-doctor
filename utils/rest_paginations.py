from rest_framework.pagination import CursorPagination

__author__ = 'FERIne'


class LargeResultsSetPagination(CursorPagination):
    page_size = 1000
    cursor_query_param = 'cursor'


class StandardResultsSetPagination(CursorPagination):
    page_size = 100
    cursor_query_param = 'cursor'


class SmallResultsSetPagination(CursorPagination):
    page_size = 10
    cursor_query_param = 'cursor'
