from datetime import timedelta

from dateutil.parser import parse
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models.signals import post_delete, post_save
from django.dispatch.dispatcher import receiver

DATE_FORMAT = "%Y-%m-%d"
TIME_FORMAT = "%H:%M:%S"


class Schedule(models.Model):
    doctor = models.OneToOneField('account.Doctor', related_name='schedule')
    visit_time = models.IntegerField(default=15)

    @property
    def request_numbers(self):
        return ScheduleSlotRequest.objects.filter(slot__schedule=self).count()

    @staticmethod
    def check_slots_availability(day, start_time, finish_time):
        start_times = ScheduleSlot.objects.filter(
            day=day, start_time__gte=start_time, start_time__lt=finish_time)
        finish_times = ScheduleSlot.objects.filter(
            day=day, finish_time__gt=start_time, finish_time__lte=finish_time)
        if start_times.exists() or finish_times.exists():
            raise ValidationError('You already set work time in this range!')

    def create_slots(self, day, start_time, finish_time):
        self.check_slots_availability(day, start_time, finish_time)
        day = parse(day)
        start_time = parse(start_time)
        start_time = timedelta(hours=start_time.hour, minutes=start_time.minute)
        finish_time = parse(finish_time)
        finish_time = timedelta(hours=finish_time.hour, minutes=finish_time.minute)
        while start_time < finish_time:
            next_time = start_time + timedelta(minutes=self.visit_time)
            ScheduleSlot.objects.create(
                schedule=self, day=day, start_time=str(start_time), finish_time=str(next_time))
            start_time = next_time

    def __str__(self):
        return '%s' % self.doctor


class ScheduleSlot(models.Model):
    schedule = models.ForeignKey('Schedule', related_name='slots')
    day = models.DateField()
    start_time = models.TimeField()
    finish_time = models.TimeField()

    class Meta:
        unique_together = ('schedule', 'day', 'start_time')

    @property
    def patient(self):
        return self.reserve.patient if hasattr(self, 'reserve') else None

    @property
    def is_reserved(self):
        return True if self.patient else False

    def __str__(self):
        return '%s - %s : %s - %s' % (
            self.schedule.doctor,
            self.day.strftime("%s" % (DATE_FORMAT,)),
            self.start_time.strftime("%s" % (TIME_FORMAT,)),
            self.finish_time.strftime("%s" % (TIME_FORMAT,)),
        )


class ScheduleSlotReserved(models.Model):
    created = models.DateTimeField(auto_now_add=True, unique=True)
    patient = models.ForeignKey(User, related_name='reserves')
    slot = models.OneToOneField('ScheduleSlot', related_name='reserve')

    def cancel(self, user):
        if user == self.slot.schedule.doctor.member.user:
            ScheduleSlotFailedReserve.objects.create(
                patient=self.patient,
                doctor=self.slot.schedule.doctor,
                day=self.slot.day,
                start_time=self.slot.start_time,
                finish_time=self.slot.finish_time
            )
            self.delete()


class ScheduleSlotRequest(models.Model):
    created = models.DateTimeField(auto_now_add=True, unique=True)
    patient = models.ForeignKey(User, related_name='reserve_requests')
    slot = models.ForeignKey('ScheduleSlot', related_name='reserve_requests')

    def __init__(self, *args, **kwargs):
        super(ScheduleSlotRequest, self).__init__(*args, **kwargs)
        self._must_delete = True

    def accept(self, user):
        if user == self.slot.schedule.doctor.member.user:
            self._must_delete = False
            self.delete()
            ScheduleSlotReserved.objects.create(
                patient=self.patient,
                slot=self.slot
            )

    def reject(self, user):
        if user == self.slot.schedule.doctor.member.user:
            ScheduleSlotFailedReserve.objects.create(
                patient=self.patient,
                doctor=self.slot.schedule.doctor,
                day=self.slot.day,
                start_time=self.slot.start_time,
                finish_time=self.slot.finish_time
            )
            self.delete()


class ScheduleSlotFailedReserve(models.Model):
    created = models.DateTimeField(auto_now_add=True, unique=True)
    patient = models.ForeignKey(User, related_name='reserve_fails')
    doctor = models.ForeignKey('account.Doctor', related_name='+')
    day = models.DateField()
    start_time = models.TimeField()
    finish_time = models.TimeField()


@receiver(post_save, sender=ScheduleSlotReserved)
def update_failed_reserves_on_reserved(instance, **kwargs):
    ScheduleSlotRequest.objects.filter(slot=instance.slot).delete()


@receiver(post_delete)
def update_failed_reserves_on_delete(sender, instance, **kwargs):
    list_of_models = ['ScheduleSlotRequest', 'ScheduleSlotReserved']
    if sender.__name__ in list_of_models:
        if getattr(instance, '_must_delete', True):
            ScheduleSlotFailedReserve.objects.create(
                patient=instance.patient,
                doctor=instance.slot.schedule.doctor,
                day=instance.slot.day,
                start_time=instance.slot.start_time,
                finish_time=instance.slot.finish_time,
            )
