from django.contrib import admin

from schedule.models import Schedule, ScheduleSlot, \
    ScheduleSlotRequest, ScheduleSlotReserved, ScheduleSlotFailedReserve

admin.site.register(Schedule)
admin.site.register(ScheduleSlot)
admin.site.register(ScheduleSlotReserved)
admin.site.register(ScheduleSlotRequest)
admin.site.register(ScheduleSlotFailedReserve)
