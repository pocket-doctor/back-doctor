from account.serializers.doctor_serializers import DoctorSerializer
from account.serializers.user_serializers import UserSerializer
from schedule.models import Schedule, ScheduleSlot, ScheduleSlotReserved, \
    ScheduleSlotRequest, ScheduleSlotFailedReserve
from utils.base_serializer import BaseRestSerializer


class ScheduleSerializer(BaseRestSerializer):
    class Meta:
        model = Schedule
        fields = ['visit_time']


class ScheduleSlotSerializer(BaseRestSerializer):
    class Meta:
        model = ScheduleSlot
        fields = ['id', 'day', 'start_time', 'finish_time', 'is_reserved']


class ScheduleSlotReservedSerializer(BaseRestSerializer):
    slot = ScheduleSlotSerializer(many=False, required=False)
    patient = UserSerializer(many=False, required=False)

    class Meta:
        model = ScheduleSlotReserved
        fields = ['created', 'slot', 'patient']
        read_only_fields = ['created']


class ScheduleSlotRequestSerializer(BaseRestSerializer):
    slot = ScheduleSlotSerializer(many=False, required=False)
    patient = UserSerializer(many=False, required=False)

    class Meta:
        model = ScheduleSlotRequest
        fields = ['created', 'slot', 'patient']
        read_only_fields = ['created']


class ScheduleSlotFailedReserveSerializer(BaseRestSerializer):
    doctor = DoctorSerializer(many=False)

    class Meta:
        model = ScheduleSlotFailedReserve
        fields = ['created', 'doctor', 'day', 'start_time', 'finish_time']
        read_only_fields = ['created']
