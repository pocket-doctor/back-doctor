from django.conf.urls import url, include

from schedule.views.appointments_views import AppointmentsByDayView, AppointmentCancelView
from schedule.views.request_views import RequestShowAllView, RequestAcceptView, RequestRejectView
from schedule.views.schedule_views import DoctorScheduleView, DoctorAddScheduleView
from schedule.views.show_schedule_views import ScheduleViews
from schedule.views.slot_views import RequestSlotViews, ReservedSlotViews, FailedSlotViews

mobile_slots_urlpatterns = [
    url(r'^$', ScheduleViews.as_view({'get': 'days'}), name='days'),
    url(r'^(?P<day>[^w]+)$', ScheduleViews.as_view({'get': 'list'}), name='day'),
]

mobile_slot_urlpatterns = [
    url(r'^request/$', RequestSlotViews.as_view({'post': 'create'}), name='request'),
    url(r'^cancel-request/$', RequestSlotViews.as_view(
        {'post': 'destroy'}), name='cancel-request'),
    url(r'^cancel-reserve/$', ReservedSlotViews.as_view(
        {'post': 'destroy'}), name='cancel-reserve'),
]

mobile_urlpatterns = [
    url(r'^slots/(?P<schedule>\d+)/', include(mobile_slots_urlpatterns, namespace='slots')),
    url(r'^(?P<slot>\d+)/', include(mobile_slot_urlpatterns, namespace='slot')),
    url(r'^requests/$', RequestSlotViews.as_view({'get': 'list'}), name='requests'),
    url(r'^reserves/$', ReservedSlotViews.as_view({'get': 'list'}), name='reserves'),
    url(r'^failed-requests/$', FailedSlotViews.as_view({'get': 'list'}), name='failed-requests'),
]

request_urlpatterns = [
    url(r'^show-all/$', RequestShowAllView.as_view(), name='show_all'),
    url(r'^(?P<id>\d+)/accept/$', RequestAcceptView.as_view(), name='accept'),
    url(r'^(?P<id>\d+)/reject/$', RequestRejectView.as_view(), name='reject'),
]

appointments_urlpatterns = [
    url(r'^$', AppointmentsByDayView.as_view(), name='show_all'),
    url(r'^(?P<year>\d+)?/(?P<month>\d+)?/(?P<day>\d+)?/$',
        AppointmentsByDayView.as_view(), name='filter'),
    url(r'^(?P<id>\d+)/cancel/$', AppointmentCancelView.as_view(), name='cancel'),
]

urlpatterns = [
    url(r'^show/$', DoctorScheduleView.as_view(), name='show'),
    url(r'^add/$', DoctorAddScheduleView.as_view(), name='add'),
    url(r'^request/', include(request_urlpatterns, namespace='request')),
    url(r'^appointments/', include(appointments_urlpatterns, namespace='appointments')),
]
