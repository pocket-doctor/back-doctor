from django.shortcuts import render
from django.urls.base import reverse
from django.views.generic.base import View
from django.views.generic.edit import CreateView

from schedule.forms.schedule_forms import AddScheduleForm
from schedule.models import ScheduleSlot


class DoctorScheduleView(View):
    @staticmethod
    def get(request):
        user = request.user
        doctor = user.member.doctor
        schedule = doctor.schedule
        data = [
            {'label': 'Visit Time', 'data': schedule.visit_time},
            {'label': 'Schedule Work Days', 'data': schedule.slots.all()},
        ]
        return render(request, 'schedule/show-schedule.html', {'data': data})


class DoctorAddScheduleView(CreateView):
    model = ScheduleSlot
    template_name = 'schedule/add-schedule.html'
    form_class = AddScheduleForm

    def get_form(self, form_class=None):
        form = super(DoctorAddScheduleView, self).get_form(form_class)
        form.schedule = self.request.user.member.doctor.schedule
        return form

    def get_success_url(self):
        return reverse('schedule:show')
