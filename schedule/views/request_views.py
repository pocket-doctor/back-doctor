from django.shortcuts import redirect
from django.urls.base import reverse
from django.views.generic.base import View
from django.views.generic.list import ListView

from schedule.models import ScheduleSlotRequest


class RequestShowAllView(ListView):
    model = ScheduleSlotRequest
    template_name = 'schedule/show-requests.html'

    def get_queryset(self):
        return ScheduleSlotRequest.objects.filter(
            slot__schedule__doctor__member__user=self.request.user).order_by('-created')


class RequestAcceptView(View):
    def get(self, request, **kwargs):
        try:
            slot_request = ScheduleSlotRequest.objects.get(id=kwargs.get('id', None))
            slot_request.accept(request.user)
        except Exception as e:
            print(e)
        return redirect(reverse('schedule:request:show_all'))


class RequestRejectView(View):
    def get(self, request, **kwargs):
        try:
            slot_request = ScheduleSlotRequest.objects.get(id=kwargs.get('id', None))
            slot_request.reject(request.user)
        except Exception as e:
            print(e)
        return redirect(reverse('schedule:request:show_all'))
