from datetime import date

from django.http.response import Http404
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from schedule.models import Schedule
from schedule.serializers.schedule_serializers import ScheduleSlotSerializer
from schedule.utils import ScheduleSlotSetPagination
from utils.base_rest_views import BaseRestViews


class ScheduleViews(BaseRestViews):
    pagination_class = ScheduleSlotSetPagination
    serializer_class = ScheduleSlotSerializer
    lookup_fields = {
        'days': ('schedule',),
        'list': ('schedule', 'day')
    }
    http_method_names = ['get']
    permission_classes = {
        'days': [IsAuthenticated],
        'list': [IsAuthenticated],
    }

    def __init__(self, **kwargs):
        self.schedule = None
        self.day = None
        self.slots = None
        super(ScheduleViews, self).__init__(**kwargs)

    def initial(self, request, *args, **kwargs):
        self.schedule = kwargs.get('schedule', None)
        if self.schedule:
            self.schedule = get_object_or_404(Schedule, pk=self.schedule)
        self.day = kwargs.get('day', None)
        if self.day and self.schedule:
            try:
                self.slots = self.schedule.slots.filter(day=self.day)
            except Exception:
                raise Http404
        return super(ScheduleViews, self).initial(request, *args, **kwargs)

    def get_queryset(self):
        return self.slots

    def days(self, request, *args, **kwargs):
        days = self.schedule.slots \
            .filter(day__gte=date.today().strftime("%Y-%m-%d")) \
            .order_by('day') \
            .values_list('day', flat=True) \
            .distinct()
        return Response(days)
