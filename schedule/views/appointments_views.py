from django.shortcuts import redirect
from django.urls.base import reverse
from django.views.generic.base import View
from django.views.generic.list import ListView

from schedule.models import ScheduleSlotReserved


class AppointmentsByDayView(ListView):
    model = ScheduleSlotReserved
    template_name = 'schedule/show-appointments.html'

    def get_queryset(self):
        filter_args = {'slot__schedule__doctor__member__user': self.request.user}
        for key in self.kwargs:
            if self.kwargs[key]:
                filter_args['slot__day__' + key] = self.kwargs[key]
        queryset = ScheduleSlotReserved.objects.filter(**filter_args)
        return queryset.order_by('-created')


class AppointmentCancelView(View):
    def get(self, request, **kwargs):
        try:
            slot_reserved = ScheduleSlotReserved.objects.get(id=kwargs.get('id', None))
            slot_reserved.cancel(request.user)
        except Exception as e:
            print(e)
        return redirect(reverse('schedule:appointments:show_all'))
