from rest_framework.exceptions import ValidationError
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated

from schedule.models import ScheduleSlot, ScheduleSlotRequest, ScheduleSlotReserved, \
    ScheduleSlotFailedReserve
from schedule.serializers.schedule_serializers import ScheduleSlotRequestSerializer, \
    ScheduleSlotFailedReserveSerializer, ScheduleSlotReservedSerializer
from utils.base_rest_views import BaseRestViews


class RequestSlotViews(BaseRestViews):
    serializer_class = ScheduleSlotRequestSerializer
    http_method_names = ['get', 'post']
    permission_classes = {
        'create': [IsAuthenticated],
        'list': [IsAuthenticated],
        'destroy': [IsAuthenticated],
    }

    def __init__(self, **kwargs):
        self.slot = None
        super(RequestSlotViews, self).__init__(**kwargs)

    def initial(self, request, *args, **kwargs):
        self.slot = kwargs.get('slot', None)
        if self.slot:
            self.slot = get_object_or_404(ScheduleSlot, id=self.slot)
        return super(RequestSlotViews, self).initial(request, *args, **kwargs)

    def get_object(self):
        return get_object_or_404(ScheduleSlotRequest, patient=self.request.user, slot=self.slot)

    def get_queryset(self):
        return ScheduleSlotRequest.objects.filter(patient=self.request.user)

    def perform_create(self, serializer):
        if not ScheduleSlotRequest.objects.filter(
                slot=self.slot, patient=self.request.user).exists():
            serializer.save(slot=self.slot, patient=self.request.user)
        else:
            raise ValidationError("Request already sent")


class ReservedSlotViews(BaseRestViews):
    serializer_class = ScheduleSlotReservedSerializer
    http_method_names = ['get', 'post']
    permission_classes = {
        'list': [IsAuthenticated],
        'destroy': [IsAuthenticated],
    }

    def __init__(self, **kwargs):
        self.slot = None
        super(ReservedSlotViews, self).__init__(**kwargs)

    def initial(self, request, *args, **kwargs):
        self.slot = kwargs.get('slot', None)
        if self.slot:
            self.slot = get_object_or_404(ScheduleSlot, id=self.slot)
        return super(ReservedSlotViews, self).initial(request, *args, **kwargs)

    def get_object(self):
        return get_object_or_404(ScheduleSlotReserved, patient=self.request.user, slot=self.slot)

    def get_queryset(self):
        return ScheduleSlotReserved.objects.filter(patient=self.request.user)


class FailedSlotViews(BaseRestViews):
    serializer_class = ScheduleSlotFailedReserveSerializer
    http_method_names = ['get']
    permission_classes = {
        'list': [IsAuthenticated],
    }

    def get_queryset(self):
        return ScheduleSlotFailedReserve.objects.filter(patient=self.request.user)
