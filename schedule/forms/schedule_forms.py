from django.forms.models import ModelForm

from schedule.models import ScheduleSlot


class AddScheduleForm(ModelForm):
    class Meta:
        model = ScheduleSlot
        fields = ('day', 'start_time', 'finish_time')
        labels = {'start_time': 'Start Time', 'finish_time': 'Finish Time'}

    def __init__(self, *args, **kwargs):
        super(AddScheduleForm, self).__init__(*args, **kwargs)
        self.schedule = None

    def save(self, commit=True):
        data = self.cleaned_data
        self.schedule.create_slots(
            str(data['day']), str(data['start_time']), str(data['finish_time']))
        return self.schedule
