from utils.rest_paginations import LargeResultsSetPagination


class ScheduleSlotSetPagination(LargeResultsSetPagination):
    ordering = '-start_time'
