from django.test.client import Client
from django.urls.base import reverse
from rest_framework import status

from account.models import Member, Doctor
from account.tests.base import AccountBaseTest


class ScheduleTest(AccountBaseTest):
    client_class = Client

    def __init__(self, *args, **kwargs):
        super(ScheduleTest, self).__init__(*args, **kwargs)
        self.url = None
        self.success_data = None

    def setUp(self):
        super(ScheduleTest, self).setUp()
        member = Member.objects.create(
            user=self.user1, melli_code='1111111111', phone_number='+989302341256')
        Doctor.objects.create(
            member=member, clinic_phone='0213455432', clinic_address='some where')
        self.client.login(username='hamed', password='salam12345')
        self.url = reverse('schedule:add')
        self.success_data = {
            'day': '2010-10-10',
            'start_time': '22:23:23',
            'finish_time': '23:23:23',
        }

    def test_show_schedule(self):
        response = self.client.get(reverse('schedule:show'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_add_schedule(self):
        response = self.client.post(self.url, self.success_data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
