from rest_framework import status
from rest_framework.reverse import reverse

from schedule.models import Schedule
from schedule.tests.base import ScheduleBaseTest

__author__ = 'FERIne'


class RequestSlotMobileTest(ScheduleBaseTest):
    def __init__(self, *args, **kwargs):
        super(RequestSlotMobileTest, self).__init__(*args, **kwargs)
        self.url = None

    def setUp(self):
        super(RequestSlotMobileTest, self).setUp()
        Schedule.objects.all()[0].create_slots("2117-10-10", "8:00", "8:15")
        self.url = reverse('api:schedule:slot:request', kwargs={'slot': '1'})

    def test_request_slot(self):
        self.login(self.user1)
        response = self.client.post(self.url)
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_twice_request_slot(self):
        self.test_request_slot()
        response = self.client.post(self.url)
        print(response.data)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data, ["Request already sent"])
