from django.core.exceptions import ValidationError

from schedule.models import Schedule, ScheduleSlot, ScheduleSlotRequest, \
    ScheduleSlotReserved, ScheduleSlotFailedReserve
from schedule.tests.base import ScheduleBaseTest

__author__ = 'FERIne'


class UserModelTest(ScheduleBaseTest):
    def __init__(self, *args, **kwargs):
        super(UserModelTest, self).__init__(*args, **kwargs)
        self.day = "2117-02-01"

    def test_create_schedule(self):
        self.assertEqual(Schedule.objects.all().count(), 1)

    def test_create_schedule_slots(self):
        schedule = Schedule.objects.all()[0]
        schedule.create_slots(self.day, "8:00", "16:00")
        self.assertEqual(ScheduleSlot.objects.all().count(), 8 * 4)

    def test_create_schedule_slots_fail(self):
        self.test_create_schedule_slots()
        self.assertRaises(ValidationError, Schedule.objects.all()[0].create_slots,
                          day=self.day, start_time="7:00", finish_time="12:00")

    def test_request_slot(self):
        self.test_create_schedule_slots()
        slot = ScheduleSlot.objects.all()[0]
        ScheduleSlotRequest.objects.create(slot=slot, patient=self.user1)
        ScheduleSlotRequest.objects.create(slot=slot, patient=self.user2)
        self.assertEqual(ScheduleSlotRequest.objects.all().count(), 2)

    def test_accept_request_slot(self):
        self.test_request_slot()
        self.assertEqual(ScheduleSlot.objects.all()[0].is_reserved, False)
        request = ScheduleSlotRequest.objects.get(patient=self.user2)
        request.accept(self.user1)
        self.assertEqual(ScheduleSlotRequest.objects.all().count(), 0)
        self.assertEqual(ScheduleSlotReserved.objects.all().count(), 1)
        self.assertEqual(ScheduleSlotFailedReserve.objects.all().count(), 1)
        self.assertEqual(ScheduleSlot.objects.all()[0].is_reserved, True)

    def test_delete_slot(self):
        self.test_accept_request_slot()
        ScheduleSlot.objects.all()[0].delete()
        self.assertEqual(ScheduleSlotReserved.objects.all().count(), 0)
        self.assertEqual(ScheduleSlotFailedReserve.objects.all().count(), 2)
