from rest_framework import status
from rest_framework.reverse import reverse

from schedule.models import Schedule, ScheduleSlotRequest
from schedule.tests.base import ScheduleBaseTest

__author__ = 'FERIne'


class NotificationMobileTest(ScheduleBaseTest):
    def __init__(self, *args, **kwargs):
        super(NotificationMobileTest, self).__init__(*args, **kwargs)
        self.requests_url = None
        self.reserves_url = None
        self.failed_requests_url = None

    def setUp(self):
        super(NotificationMobileTest, self).setUp()
        Schedule.objects.all()[0].create_slots("2117-10-10", "8:00", "9:00")
        self.login(self.user2)
        self.client.post(reverse('api:schedule:slot:request', kwargs={'slot': '1'}))
        self.client.post(reverse('api:schedule:slot:request', kwargs={'slot': '2'}))
        self.client.post(reverse('api:schedule:slot:request', kwargs={'slot': '3'}))
        ScheduleSlotRequest.objects.all()[0].accept(self.user1)
        ScheduleSlotRequest.objects.all()[0].delete()
        self.requests_url = reverse('api:schedule:requests')
        self.reserves_url = reverse('api:schedule:reserves')
        self.failed_requests_url = reverse('api:schedule:failed-requests')

    def test_requests_notifications(self):
        response = self.client.get(self.requests_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    def test_reserves_notifications(self):
        response = self.client.get(self.reserves_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)

    def test_failed_requests_notifications(self):
        response = self.client.get(self.failed_requests_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data['results']), 1)
