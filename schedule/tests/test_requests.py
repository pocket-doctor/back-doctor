from django.test.client import Client
from django.urls.base import reverse
from rest_framework import status

from account.models import Doctor
from schedule.models import ScheduleSlot, ScheduleSlotRequest
from schedule.tests.base import ScheduleBaseTest


class RequestTest(ScheduleBaseTest):
    client_class = Client

    def __init__(self, *args, **kwargs):
        super(RequestTest, self).__init__(*args, **kwargs)
        self.request = None

    def setUp(self):
        super(RequestTest, self).setUp()
        doctor = Doctor.objects.all()[0]
        doctor.schedule.create_slots('2017-10-10', '8:00', '16:00')
        slot = ScheduleSlot.objects.all()[0]
        self.request = ScheduleSlotRequest.objects.create(slot=slot, patient=self.user2)
        self.client.login(username='hamed', password='salam12345')

    def test_show_all_requests(self):
        response = self.client.get(reverse('schedule:request:show_all'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_reject_request(self):
        response = self.client.get(reverse('schedule:request:reject',
                                           kwargs={'id': self.request.id}))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_reject_request_bad(self):
        response = self.client.get(reverse('schedule:request:reject',
                                           kwargs={'id': 25}))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_accept_request(self):
        response = self.client.get(reverse('schedule:request:accept',
                                           kwargs={'id': self.request.id}))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_accept_request_bad(self):
        response = self.client.get(reverse('schedule:request:accept',
                                           kwargs={'id': 25}))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
