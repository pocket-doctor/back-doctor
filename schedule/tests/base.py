from account.models import Member, Doctor
from account.tests.base import AccountBaseTest

__author__ = 'FERIne'


class ScheduleBaseTest(AccountBaseTest):
    def setUp(self):
        super(ScheduleBaseTest, self).setUp()
        member = Member.objects.create(
            user=self.user1, melli_code='1111111111', phone_number='+989302341256')
        Doctor.objects.create(
            member=member, clinic_phone='0213455432', clinic_address='some where')
