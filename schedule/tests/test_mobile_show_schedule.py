from collections import OrderedDict
from datetime import date

from rest_framework import status
from rest_framework.reverse import reverse

from schedule.models import Schedule
from schedule.tests.base import ScheduleBaseTest

__author__ = 'FERIne'


class ShowScheduleMobileTest(ScheduleBaseTest):
    def __init__(self, *args, **kwargs):
        super(ShowScheduleMobileTest, self).__init__(*args, **kwargs)
        self.day_url = None
        self.days_url = None
        self.success_day_data = None
        self.success_days_data = None
        self.day = "2117-02-01"

    def setUp(self):
        super(ShowScheduleMobileTest, self).setUp()
        Schedule.objects.all()[0].create_slots(self.day, "8:00", "8:15")
        self.day_url = reverse('api:schedule:slots:day',
                               kwargs={'schedule': '1', 'day': self.day})
        self.days_url = reverse('api:schedule:slots:days',
                                kwargs={'schedule': '1'})
        self.success_day_data = OrderedDict([
            ('next', None),
            ('previous', None),
            ('results', [
                OrderedDict([
                    ("id", 1),
                    ("day", self.day),
                    ("start_time", "08:00:00"),
                    ("finish_time", "08:15:00"),
                    ("is_reserved", False),
                ])
            ])
        ])
        self.success_days_data = [date(year=2117, month=2, day=1)]

    def test_get_days(self):
        self.login(self.user1)
        response = self.client.get(self.days_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(list(response.data), self.success_days_data)

    def test_get_days_fail(self):
        self.login(self.user1)
        response = self.client.get(reverse('api:schedule:slots:days',
                                           kwargs={'schedule': '2'}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_get_day(self):
        self.login(self.user1)
        response = self.client.get(self.day_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.success_day_data)

    def test_get_day_fail(self):
        self.login(self.user1)
        response = self.client.get(reverse('api:schedule:slots:day',
                                           kwargs={'schedule': '1', 'day': "2"}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
