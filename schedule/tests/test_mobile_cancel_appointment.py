from rest_framework import status
from rest_framework.reverse import reverse

from schedule.models import Schedule, ScheduleSlotRequest
from schedule.tests.base import ScheduleBaseTest

__author__ = 'FERIne'


class CancelAppointmentMobileTest(ScheduleBaseTest):
    def __init__(self, *args, **kwargs):
        super(CancelAppointmentMobileTest, self).__init__(*args, **kwargs)
        self.url = None

    def setUp(self):
        super(CancelAppointmentMobileTest, self).setUp()
        Schedule.objects.all()[0].create_slots("2117-10-10", "8:00", "8:15")
        self.url = reverse('api:schedule:slot:request', kwargs={'slot': '1'})
        self.login(self.user1)
        self.client.post(self.url)

    def test_cancel_request(self):
        self.url = reverse('api:schedule:slot:cancel-request', kwargs={'slot': '1'})
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)

    def test_cancel_reserve(self):
        self.url = reverse('api:schedule:slot:cancel-reserve', kwargs={'slot': '1'})
        doctor = Schedule.objects.all()[0].doctor
        ScheduleSlotRequest.objects.all()[0].accept(doctor.member.user)
        response = self.client.post(self.url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
