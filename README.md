# Pocket Doctor

### Installation

It's brand new app, please wait for it :D

### Discussion

Join the [#pocket-doctor](https://discord.gg/0ZcbPKXt5bZ6au5t) channel of the [PocketDoctor](http://www.pocket-doctor.com) Discord community.

### Thanks

* [Farnood Massoudi](https://github.com/farnoodma/) for tell us nice stories;

Special thanks to [Hamid Kazemi](https://github.com/kazemizi/) for destroying the stories for us.

### Change Log

This project adheres to [Semantic Versioning](http://semver.org/).  
Every release, along with the migration instructions, is documented on the Github [Releases](https://github.com/reactjs/redux/releases) page.

### Patrons

The work on Pocket Doctor was [funded by the community](https://www.patreon.com/pocket-doctor).  

[See the full list of Redux patrons.](PATRONS.md)

### License

MIT
