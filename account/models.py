from dateutil.parser import parse
from django.contrib.auth.models import User
from django.core.validators import RegexValidator
from django.db import models
from django.db.models.signals import post_save
from django.dispatch.dispatcher import receiver

from advertisement.models import DoctorMonthlyPromotion
from schedule.models import Schedule
from utils.media_storage import DEFAULT_STORAGE, get_file_url


class Member(models.Model):
    user = models.OneToOneField(User, related_name='member')
    melli_code_regex = RegexValidator(
        regex=r'^\d{10,11}$',
        message="Melli code must be 10 or 11 digit number.")
    melli_code = models.CharField(validators=[melli_code_regex], max_length=12, unique=True)
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message="Phone number must be entered in the format: "
                "'+999999999'. Up to 15 digits allowed.")
    phone_number = models.CharField(validators=[phone_regex], max_length=20, unique=True)

    @property
    def is_doctor(self):
        if getattr(self, 'doctor', None):
            return True
        return False

    def __str__(self):
        return self.user.username


class Doctor(models.Model):
    member = models.OneToOneField('Member', related_name='doctor')
    degree = models.CharField(max_length=100, blank=True, null=True)
    degree_year = models.DateField(blank=True, null=True)
    university = models.CharField(max_length=100, blank=True, null=True)
    phone_regex = RegexValidator(
        regex=r'^\+?1?\d{9,15}$',
        message="Phone number must be entered in the format: "
                "'+999999999'. Up to 15 digits allowed.")
    clinic_phone = models.CharField(validators=[phone_regex], max_length=20, unique=True)
    clinic_address = models.TextField(max_length=1000)
    contract = models.FileField(upload_to='contracts/', storage=DEFAULT_STORAGE)

    @property
    def contract_url(self):
        if self.contract:
            return get_file_url(self.contract.url)
        return None

    @property
    def name(self):
        return self.member.user.get_full_name()

    def promote(self, date):
        date = parse(date)
        date = date.replace(day=1).strftime("%s" % ("%Y-%m-%d",))
        DoctorMonthlyPromotion.objects.create(doctor=self, date=date)

    def __str__(self):
        return '%s' % self.member


@receiver(post_save, sender=Doctor)
def doctor_post_save(instance, **kwargs):
    try:
        Schedule.objects.get_or_create(doctor=instance)
    except Exception as e:
        instance.delete()
        raise e
