from django.conf.urls import url, include
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token

from account.views.doctor_profile_views import DoctorProfileView, DoctorEditProfileView
from account.views.doctor_views import DoctorViews
from account.views.join_views import JoinView, JoinDoctorView
from account.views.login_views import LoginView, LogoutView
from account.views.profile_views import EditProfileView, UserProfileView
from account.views.user_views import UserViews

mobile_login_urlpatterns = [
    url(r'^$', obtain_jwt_token, name='login'),
    url(r'^refresh/$', refresh_jwt_token, name='refresh'),
    url(r'^verify/$', verify_jwt_token, name='verify')
]

mobile_urlpatterns = [
    url(r'^join/$', UserViews.as_view({'post': 'create'}), name='join'),
    url(r'^login/', include(mobile_login_urlpatterns, namespace='login')),
    url(r'^profile/$', UserViews.as_view({'get': 'retrieve', 'post': 'update'}), name='profile'),
    url(r'^search/(?P<q>[^w]+)/$', DoctorViews.as_view({'get': 'list'}), name='search'),
    url(r'^doctor/(?P<id>\d+)/$', DoctorViews.as_view({'get': 'retrieve'}), name='show-doctor'),
]

profile_urlpatterns = [
    url(r'^$', UserProfileView.as_view(), name='show'),
    url(r'^edit/$', EditProfileView.as_view(), name='edit')
]

doctor_profile_urlpatterns = [
    url(r'^$', DoctorProfileView.as_view(), name='show'),
    url(r'^edit/$', DoctorEditProfileView.as_view(), name='edit')
]

urlpatterns = [
    url(r'^join/$', JoinView.as_view(), name='join'),
    url(r'^join/doctor', JoinDoctorView.as_view(), name='join-doctor'),
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^profile/', include(profile_urlpatterns, namespace='profile')),
    url(r'^doctor-profile/', include(doctor_profile_urlpatterns, namespace='doctor-profile'))
]
