from django.test.client import Client
from django.urls.base import reverse
from rest_framework import status

from account.tests.base import AccountBaseTest


class UserJoinTest(AccountBaseTest):
    client_class = Client

    def __init__(self, *args, **kwargs):
        super(UserJoinTest, self).__init__(*args, **kwargs)
        self.url = None
        self.success_data = None

    def setUp(self):
        super(UserJoinTest, self).setUp()
        self.url = reverse('account:join')
        self.success_data = {
            'melli_code': '3950293531',
            'phone_number': '+989127016105',
            'username': 'farnoodma',
            'first_name': 'farnood',
            'last_name': 'massoudi',
            'email': 'ferine.hellion@gmail.com',
            'password': 'salam12345'
        }

    def test_join(self):
        response = self.client.post(self.url, self.success_data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_invalid_username(self):
        self.success_data["username"] = "hamed"
        response = self.client.post(self.url, self.success_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_email(self):
        self.success_data["email"] = "wrong"
        response = self.client.post(self.url, self.success_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
