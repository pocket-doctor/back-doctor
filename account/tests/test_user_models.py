from unittest.mock import Mock, patch

from django.contrib.auth.models import User

from account.models import Doctor, Member
from account.tests.base import AccountBaseTest

__author__ = 'FERIne'


class ScheduleObjectsMock(Mock):
    @staticmethod
    def get_or_create(*args, **kwargs):
        raise Exception()


class ScheduleMock(Mock):
    objects = ScheduleObjectsMock()


class UserModelTest(AccountBaseTest):
    def test_create_user(self):
        farnood = User.objects.create_user("farnoodma", email="test@test.test", password="12345")
        self.assertEqual(farnood.username, "farnoodma")

    def test_delete_user(self):
        self.test_create_user()
        farnood = User.objects.get(username="farnoodma")
        accepted, _ = farnood.delete()
        self.assertEqual(accepted, 1)

    def test_create_doctor(self):
        farnood = User.objects.create_user("farnoodma", email="test@test.test", password="12345")
        self.assertEqual(farnood.__str__(), 'farnoodma')
        farnood = Member.objects.create(
            user=farnood, melli_code='1234567891', phone_number='+989127016105')
        self.assertEqual(farnood.__str__(), 'farnoodma')
        self.assertEqual(farnood.is_doctor, False)
        farnood = Doctor.objects.create(member=farnood, degree='PHD', university='Sharif',
                                        clinic_phone='0212223647', clinic_address='No where')
        self.assertEqual(farnood.__str__(), 'farnoodma')
        self.assertEqual(farnood.member.is_doctor, True)
        self.assertEqual(farnood.contract_url, None)

    def test_delete_doctor(self):
        self.test_create_doctor()
        User.objects.get(username="farnoodma").delete()
        self.assertEqual(Doctor.objects.all().count(), 0)

    @patch('account.models.Schedule', ScheduleMock())
    def test_doctor_save_fail_on_schedule_save_error(self):
        farnood = User.objects.create_user("farnoodma", email="test@test.test", password="12345")
        farnood = Member.objects.create(
            user=farnood, melli_code='1234567891', phone_number='+989127016105')
        self.assertRaises(Exception, Doctor.objects.create, member=farnood, degree='PHD',
                          university='Sharif', clinic_phone='0212223647',
                          clinic_address='No where')
        self.assertEqual(Doctor.objects.all().count(), 0)
