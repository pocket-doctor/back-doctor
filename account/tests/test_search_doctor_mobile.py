from rest_framework import status
from rest_framework.reverse import reverse

from account.models import Member, Doctor
from account.tests.base import AccountBaseTest

__author__ = 'FERIne'


class UserProfileMobileTest(AccountBaseTest):
    def __init__(self, *args, **kwargs):
        super(UserProfileMobileTest, self).__init__(*args, **kwargs)
        self.url = None
        self.success_data = None
        self.fail_data = None

    def setUp(self):
        super(UserProfileMobileTest, self).setUp()
        member = Member.objects.create(
            user=self.user1, melli_code='1234567891', phone_number='+989127016105')
        Doctor.objects.create(
            member=member, degree="PHD", degree_year="2010-01-10", university="Sharif",
            clinic_phone="0212233445", clinic_address="Some Where")
        self.url = reverse('api:account:search', kwargs={'q': 'hame-sal'})
        self.success_data = {
            'next': None,
            'previous': None,
            'results': [
                {
                    "id": 1,
                    "name": "Hamed Saleh",
                    "degree": "PHD",
                    "degree_year": "2010-01-10",
                    "university": "Sharif",
                    "clinic_phone": "0212233445",
                    "clinic_address": "Some Where",
                    "schedule": 1
                }
            ]
        }
        self.fail_data = {
            'next': None,
            'previous': None,
            'results': []
        }

    def test_search(self):
        self.login(self.user1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.success_data)

    def test_search_all(self):
        self.url = reverse('api:account:search', kwargs={'q': '*'})
        self.test_search()

    def test_search_fail(self):
        self.url = reverse('api:account:search', kwargs={'q': 'chert'})
        self.login(self.user1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.fail_data)
