from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework_jwt.settings import api_settings

from middleware.base_test_case import BaseTestCase

__author__ = 'FERIne'


class AccountBaseTest(BaseTestCase):
    def __init__(self, *args, **kwargs):
        super(AccountBaseTest, self).__init__(*args, **kwargs)
        self.user1 = None
        self.user2 = None

    def setUp(self):
        super(AccountBaseTest, self).setUp()
        self.user1 = User.objects.create_user(
            'hamed',
            email='test@test.com',
            password='salam12345',
            first_name='Hamed',
            last_name='Saleh')
        self.user2 = User.objects.create_user(
            'amin',
            email='amin@test.com',
            password='salam12345',
            first_name='Amin',
            last_name='Qiasi')

    def logout(self):
        self.jwt_token = None
        self.client.credentials()

    def login(self, user):
        self.logout()
        login_url = reverse('api:account:login:login')
        login_data = {
            'username': user.username,
            'password': 'salam12345'
        }
        response = self.client.post(login_url, login_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.jwt_token = response.data['token']
        credentials = "{0} {1}".format(
            api_settings.JWT_AUTH_HEADER_PREFIX, self.jwt_token)
        self.client.credentials(HTTP_AUTHORIZATION=credentials)
