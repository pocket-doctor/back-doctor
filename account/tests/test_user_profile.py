from django.test.client import Client
from django.urls.base import reverse
from rest_framework import status

from account.models import Member, Doctor
from account.tests.base import AccountBaseTest


class UserProfileTest(AccountBaseTest):
    client_class = Client

    def __init__(self, *args, **kwargs):
        super(UserProfileTest, self).__init__(*args, **kwargs)
        self.url = None
        self.success_data = None

    def setUp(self):
        super(UserProfileTest, self).setUp()
        member = Member.objects.create(
            user=self.user1, melli_code='1111111111', phone_number='+989302341256')
        Doctor.objects.create(
            member=member, clinic_phone='0213455432', clinic_address='some where')
        self.client.login(username='hamed', password='salam12345')
        self.url = reverse('account:profile:edit')
        self.success_data = {
            'username': 'hamed',
            'email': 'test@test.com',
            'melli_code': '1234567891',
            'phone_number': '+982344242425',
            'first_name': 'new name',
            'last_name': 'new last name'
        }

    def test_show_profile(self):
        response = self.client.get(reverse('account:profile:show'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_edit_profile(self):
        response = self.client.post(self.url, self.success_data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_show_doctor_profile(self):
        response = self.client.get(reverse('account:doctor-profile:show'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_edit_doctor_profile(self):
        response = self.client.post(reverse('account:doctor-profile:edit'), {
            'degree': 'PHD',
            'degree_year': '2010-10-10',
            'clinic_phone': '0213455432',
            'clinic_address': 'some where 2'
        })
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
