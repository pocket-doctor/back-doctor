from django.test.client import Client
from django.urls.base import reverse
from rest_framework import status

from account.tests.base import AccountBaseTest


class UserLoginTest(AccountBaseTest):
    client_class = Client

    def __init__(self, *args, **kwargs):
        super(UserLoginTest, self).__init__(*args, **kwargs)
        self.url = None
        self.success_data = None

    def setUp(self):
        super(UserLoginTest, self).setUp()
        self.url = reverse('account:login')
        self.success_data = {
            'username': self.user1.username,
            'password': 'salam12345',
        }

    def test_login(self):
        response = self.client.post(self.url, self.success_data)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_login_fail_pass(self):
        self.success_data["password"] = "chert"
        response = self.client.post(self.url, self.success_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_login_fail_user(self):
        self.success_data["username"] = "chert"
        response = self.client.post(self.url, self.success_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_logout(self):
        self.client.login(username='temporary', password='temporary')
        response = self.client.get(reverse('account:logout'))
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
