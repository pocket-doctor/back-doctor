from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.reverse import reverse

from account.tests.base import AccountBaseTest

__author__ = 'FERIne'


class UserJoinMobileTest(AccountBaseTest):
    def __init__(self, *args, **kwargs):
        super(UserJoinMobileTest, self).__init__(*args, **kwargs)
        self.url = None
        self.success_data = None

    def setUp(self):
        super(UserJoinMobileTest, self).setUp()
        self.url = reverse('api:account:join')
        self.success_data = {
            'member': {
                'melli_code': '3950293531',
                'phone_number': '+989127016105'
            },
            'username': 'farnoodma',
            'first_name': 'farnood',
            'last_name': 'massoudi',
            'email': 'ferine.hellion@gmail.com',
            'password': 'salam12345',
            'confirm_password': 'salam12345'
        }

    def test_success(self):
        response = self.client.post(self.url, self.success_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(User.objects.all().count(), 3)

    def test_invalid_password(self):
        self.success_data['confirm_password'] = 'wrong'
        response = self.client.post(self.url, self.success_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_melli_code(self):
        self.success_data['member']['melli_code'] = '395029353'
        response = self.client.post(self.url, self.success_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_phone_number(self):
        self.success_data['member']['phone_number'] = '912701'
        response = self.client.post(self.url, self.success_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_email(self):
        self.success_data['email'] = 'wrong'
        response = self.client.post(self.url, self.success_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
