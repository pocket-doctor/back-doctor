from rest_framework import status
from rest_framework.reverse import reverse

from account.tests.base import AccountBaseTest

__author__ = 'FERIne'


class UserLoginMobileTest(AccountBaseTest):
    def __init__(self, *args, **kwargs):
        super(UserLoginMobileTest, self).__init__(*args, **kwargs)
        self.url = None
        self.success_data = None

    def setUp(self):
        super(UserLoginMobileTest, self).setUp()
        self.url = reverse('api:account:login:login')
        self.success_data = {
            'username': self.user1.username,
            'password': 'salam12345',
        }

    def test_success(self):
        response = self.client.post(self.url, self.success_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_invalid_password(self):
        self.success_data['password'] = 'wrong'
        response = self.client.post(self.url, self.success_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_username(self):
        self.success_data['username'] = 'wrong'
        response = self.client.post(self.url, self.success_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
