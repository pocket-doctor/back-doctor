from rest_framework import status
from rest_framework.reverse import reverse

from account.models import Member
from account.tests.base import AccountBaseTest

__author__ = 'FERIne'


class UserProfileMobileTest(AccountBaseTest):
    def __init__(self, *args, **kwargs):
        super(UserProfileMobileTest, self).__init__(*args, **kwargs)
        self.url = None
        self.success_data_get = None
        self.success_data_post = None

    def setUp(self):
        super(UserProfileMobileTest, self).setUp()
        self.url = reverse('api:account:profile')
        self.success_data_get = {
            'member': None,
            'username': 'hamed',
            'first_name': 'Hamed',
            'last_name': 'Saleh',
            'email': 'test@test.com'
        }
        self.success_data_post = {
            'member': {
                'melli_code': '1234567891',
                'phone_number': '+982344242425'
            },
            'first_name': 'new name',
            'last_name': 'new last name'
        }

    def test_login(self):
        self.login(self.user1)

    def test_show_profile(self):
        self.login(self.user1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, self.success_data_get)

    def test_edit_profile(self):
        self.login(self.user1)
        response = self.client.post(self.url, data=self.success_data_post)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Member.objects.all().count(), 1)

    def test_edit_invalid_profile(self):
        self.login(self.user1)
        self.success_data_post['username'] = 'amin'
        response = self.client.post(self.url, data=self.success_data_post)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
