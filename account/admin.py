from django.contrib import admin

from account.models import Member, Doctor

admin.site.register(Member)
admin.site.register(Doctor)
