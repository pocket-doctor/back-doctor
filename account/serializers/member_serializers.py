from account.models import Member
from utils.base_serializer import BaseRestSerializer


class MemberSerializer(BaseRestSerializer):
    class Meta:
        model = Member
        fields = ['melli_code', 'phone_number']
