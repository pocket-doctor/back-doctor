from account.models import Doctor
from utils.base_serializer import BaseRestSerializer


class DoctorSerializer(BaseRestSerializer):
    class Meta:
        model = Doctor
        fields = ['id', 'name', 'degree', 'degree_year', 'university',
                  'clinic_phone', 'clinic_address', 'schedule']
