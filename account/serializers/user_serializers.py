from django.contrib.auth.models import User
from rest_framework import serializers

from account.serializers.member_serializers import MemberSerializer
from utils.base_serializer import BaseRestSerializer
from utils.serializer_validators import ConfirmPasswordValidator


class UserSerializer(BaseRestSerializer):
    member = MemberSerializer(required=True, many=False)
    confirm_password = serializers.CharField(required=False, write_only=True)

    class Meta:
        model = User
        fields = ['member', 'username', 'first_name', 'last_name', 'email',
                  'password', 'confirm_password']
        read_only_fields = ('id', 'is_official', 'is_verify', 'profile_picture_url',
                            'phone_token_expire_date', 'phone_is_verify',
                            'email_token_expire_date', 'email_is_verify')
        write_only_fields = ('password', 'confirm_password')
        validators = [
            ConfirmPasswordValidator(
                password='password',
                confirm_password='confirm_password'
            )
        ]

    @staticmethod
    def create_member(user, member_data):
        member_serializer = MemberSerializer(data=member_data)
        if member_serializer.is_valid():
            member_serializer.save(user=user)

    @staticmethod
    def update_member(instance, validated_data):
        member_data = validated_data.pop('member', {})
        member_serializer = MemberSerializer(instance=instance, data=member_data, partial=True)
        if member_serializer.is_valid():
            member_serializer.save()
        return validated_data

    def create(self, validated_data):
        validated_data.pop('confirm_password', None)
        member_data = validated_data.pop('member', {})
        user = User.objects.create_user(**validated_data)
        self.create_member(user, member_data)
        return user

    def update(self, instance, validated_data):
        validated_data.pop('confirm_password', None)
        if getattr(instance, 'member', None):
            validated_data = self.update_member(instance.member, validated_data)
        else:
            self.create_member(instance, validated_data.pop('member', {}))
        return super(UserSerializer, self).update(instance, validated_data)
