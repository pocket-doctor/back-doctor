from rest_framework.permissions import IsAuthenticated

from account.serializers.user_serializers import UserSerializer
from utils.base_rest_views import BaseRestViews
from utils.serializer_permissions import IsNotUser


class UserViews(BaseRestViews):
    serializer_class = UserSerializer
    http_method_names = ['get', 'post']
    permission_classes = {
        'create': [IsNotUser],
        'retrieve': [IsAuthenticated],
        'update': [IsAuthenticated],
    }

    def get_object(self):
        return self.request.user
