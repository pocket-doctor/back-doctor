import re

from django.db.models import Q
from rest_framework.permissions import IsAuthenticated

from account.models import Doctor
from account.serializers.doctor_serializers import DoctorSerializer
from account.utils import DoctorSetPagination
from utils.base_rest_views import BaseRestViews


class DoctorViews(BaseRestViews):
    pagination_class = DoctorSetPagination
    serializer_class = DoctorSerializer
    lookup_fields = {
        'retrieve': ('id',),
        'list': ('q',)
    }
    http_method_names = ['get']
    permission_classes = {
        'retrieve': [IsAuthenticated],
        'list': [IsAuthenticated],
    }

    def __init__(self, **kwargs):
        self.query = None
        super(DoctorViews, self).__init__(**kwargs)

    def initial(self, request, *args, **kwargs):
        self.query = kwargs.get('q', '*')
        return super(DoctorViews, self).initial(request, *args, **kwargs)

    def get_queryset(self):
        if self.query == '*':
            return Doctor.objects.all()
        db_filter = Q(member__user__first_name__icontains=self.query) | Q(
            member__user__last_name__icontains=self.query)
        for query in re.split('[\n `~!@#$%^&*()_\-+=\'";:?/>.<,]+', self.query):
            db_filter |= Q(member__user__first_name__icontains=query) | Q(
                member__user__last_name__icontains=query)
        return Doctor.objects.filter(db_filter)
