from django.shortcuts import render
from django.urls.base import reverse
from django.views.generic.base import View
from django.views.generic.edit import UpdateView

from account.forms.edit_profile_form import EditDoctorProfileForm
from account.models import Doctor


class DoctorProfileView(View):
    def get(self, request):
        user = request.user
        doctor = user.member.doctor
        data = [
            {'label': 'Degree', 'data': doctor.degree},
            {'label': 'Degree Year', 'data': doctor.degree_year},
            {'label': 'University', 'data': doctor.university},
            {'label': 'Clinic Phone', 'data': doctor.clinic_phone},
            {'label': 'Clinic Address', 'data': doctor.clinic_address},
            {'label': 'Your Contract', 'data': doctor.contract_url},
        ]
        return render(request, 'account/doctor-profile.html', {'data': data})


class DoctorEditProfileView(UpdateView):
    model = Doctor
    template_name = 'account/doctor-edit_profile.html'
    form_class = EditDoctorProfileForm
    success_message = 'Your changes have been saved successfully!'

    def get_object(self, queryset=None):
        obj = self.request.user.member.doctor
        return obj

    def get_success_url(self):
        return reverse('account:doctor-profile:show')
