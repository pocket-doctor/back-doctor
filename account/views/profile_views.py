from django.contrib.auth.models import User
from django.core.exceptions import SuspiciousOperation
from django.shortcuts import render
from django.urls.base import reverse
from django.views.generic.base import View
from django.views.generic.edit import UpdateView

from account.forms.edit_profile_form import EditProfileForm


class UserProfileView(View):
    def get(self, request):
        user = request.user
        member = user.member
        data = [
            {'label': 'Username', 'data': user.username},
            {'label': 'Email', 'data': user.email},
            {'label': 'First Name', 'data': user.first_name},
            {'label': 'Last Name', 'data': user.last_name},
            {'label': 'Melli Code', 'data': member.melli_code},
            {'label': 'Phone Number', 'data': member.phone_number},
        ]
        return render(request, 'account/profile.html', {'data': data})


class EditProfileView(UpdateView):
    model = User
    template_name = 'account/edit_profile.html'
    form_class = EditProfileForm
    success_message = 'Your changes have been saved successfully!'

    def get_object(self, queryset=None):
        obj = self.request.user
        return obj

    def get_form(self, form_class=None):
        form = super(EditProfileView, self).get_form(form_class)
        form.member = self.request.user.member
        return form

    def get_success_url(self):
        return reverse('account:profile:show')

    def get_initial(self):
        initial = super(EditProfileView, self).get_initial()

        if self.object != self.request.user:
            raise SuspiciousOperation()

        initial.update({'melli_code': self.object.member.melli_code,
                        'phone_number': self.object.member.phone_number})
        return initial
