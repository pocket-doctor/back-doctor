from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User
from django.urls.base import reverse
from django.views.generic.edit import CreateView

from account.forms.join_forms import JoinForm, DoctorJoinForm
from account.models import Doctor


class JoinView(CreateView):
    model = User
    template_name = 'account/join.html'
    form_class = JoinForm

    def form_valid(self, form):
        ret = super(JoinView, self).form_valid(form)
        data = form.cleaned_data
        user = authenticate(
            username=self.object.username, password=data['password'])
        login(self.request, user)
        return ret

    def get_success_url(self):
        return reverse('home')


class JoinDoctorView(CreateView):
    model = Doctor
    template_name = 'account/join_doctor.html'
    form_class = DoctorJoinForm

    def get_form_kwargs(self, **kwargs):
        form_kwargs = super(JoinDoctorView, self).get_form_kwargs()
        form_kwargs['user'] = self.request.user
        return form_kwargs

    def get_success_url(self):
        return reverse('home')
