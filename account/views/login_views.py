from django.conf import settings
from django.contrib.auth import authenticate, logout
from django.contrib.auth import login
from django.contrib.auth.models import User
from django.shortcuts import redirect, resolve_url
from django.utils.http import is_safe_url
from django.views.generic.base import View
from django.views.generic.edit import FormView

from account.forms.login_forms import LoginForm


class LoginView(FormView):
    template_name = 'account/login.html'
    form_class = LoginForm

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return redirect(resolve_url(settings.LOGIN_REDIRECT_URL))
        return super(LoginView, self).get(self, request, *args, **kwargs)

    def form_valid(self, form):
        data = form.cleaned_data
        user = User.objects.get(username=data['username'])
        user = authenticate(
            username=user.username, password=data['password'])

        redirect_to = self.request.GET.get('next', '')
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

        login(self.request, user)
        return redirect(redirect_to)


class LogoutView(View):
    def get(self, request):
        redirect_to = self.request.GET.get('next', '')
        if not is_safe_url(url=redirect_to, host=self.request.get_host()):
            redirect_to = resolve_url(settings.LOGIN_URL)

        logout(request)

        return redirect(redirect_to)
