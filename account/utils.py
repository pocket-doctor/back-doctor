from utils.rest_paginations import SmallResultsSetPagination


class DoctorSetPagination(SmallResultsSetPagination):
    ordering = 'id'
