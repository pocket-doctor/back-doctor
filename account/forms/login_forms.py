from django import forms
from django.contrib.auth.models import User
from django.forms import Form
from django.utils.translation import ugettext_lazy as _


class LoginForm(Form):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

    def clean_password(self):
        data = self.cleaned_data
        username = data.get('username', None)
        user = User.objects.filter(username=username).all()
        if not user:
            raise forms.ValidationError(_('Username or Password is incorrect'))
        password = data.get('password', None)
        if not user[0].check_password(password):
            raise forms.ValidationError(_('Username or Password is incorrect'))
        return password
