from django import forms
from django.contrib.auth.models import User
from django.forms import ModelForm, fields_for_model
from django.utils.translation import ugettext_lazy as _

from account.models import Member, Doctor


class EditProfileForm(ModelForm):
    melli_code = forms.CharField(
        widget=forms.TextInput(),
        max_length=fields_for_model(Member)['melli_code'].max_length,
        label=_('Melli Code')
    )
    phone_number = forms.CharField(
        widget=forms.TextInput(),
        max_length=fields_for_model(Member)['phone_number'].max_length,
        label=_('Phone Number')
    )

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'melli_code', 'phone_number')
        labels = {'first_name': _('First Name'), 'last_name': _('Last Name')}


class EditDoctorProfileForm(ModelForm):
    class Meta:
        model = Doctor
        fields = ('degree', 'degree_year', 'university', 'clinic_phone', 'clinic_address')
        labels = {'degree_year': _('Degree Year'),
                  'clinic_phone': _('Clinic Phone'), 'clinic_address': _('Clinic Address')}
