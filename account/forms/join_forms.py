from django import forms
from django.contrib.auth.models import User
from django.forms.models import ModelForm, fields_for_model

from account.models import Member, Doctor


class JoinForm(ModelForm):
    melli_code = fields_for_model(Member)['melli_code']
    phone_number = fields_for_model(Member)['phone_number']

    def __init__(self, *args, **kwargs):
        super(JoinForm, self).__init__(*args, **kwargs)
        self.fields['password'].widget = forms.PasswordInput()
        self.fields['email'].required = True

    class Meta:
        model = User
        fields = ('username', 'email', 'first_name', 'last_name', 'password')

    def clean_melli_code(self):
        melli_code = self.cleaned_data['melli_code']
        try:
            Member.objects.get(melli_code=melli_code)
            raise forms.ValidationError('Your melli code already exists')
        except Member.DoesNotExist:
            return melli_code

    def clean_phone_number(self):
        phone_number = self.cleaned_data['phone_number']
        try:
            Member.objects.get(phone_number=phone_number)
            raise forms.ValidationError('Your phone number already exists')
        except Member.DoesNotExist:
            return phone_number

    def save(self, commit=True):
        data = self.cleaned_data
        user = User.objects.create_user(username=data['username'],
                                        password=data['password'],
                                        email=data['email'],
                                        first_name=data['first_name'],
                                        last_name=data['last_name'])
        Member.objects.create(user=user,
                              melli_code=data['melli_code'],
                              phone_number=data['phone_number'])
        return user


class DoctorJoinForm(ModelForm):
    degree = fields_for_model(Doctor)['degree']
    degree_year = fields_for_model(Doctor)['degree_year']
    university = fields_for_model(Doctor)['university']
    clinic_phone = fields_for_model(Doctor)['clinic_phone']
    clinic_address = fields_for_model(Doctor)['clinic_address']
    contract = fields_for_model(Doctor)['contract']

    def __init__(self, user, *args, **kwargs):
        self.member = Member.objects.get(user=user)
        super(DoctorJoinForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Doctor
        fields = ('degree', 'degree_year', 'university',
                  'clinic_phone', 'clinic_address', 'contract')

    def clean_phone_number(self):
        clinic_phone = self.cleaned_data['clinic_phone']
        try:
            Doctor.objects.get(clinic_phone=clinic_phone)
            raise forms.ValidationError('Your clinic phone number already exists')
        except Doctor.DoesNotExist:
            return clinic_phone

    def clean(self):
        try:
            Doctor.objects.get(member=self.member)
            raise forms.ValidationError('You are already a doctor')
        except Doctor.DoesNotExist:
            return super(DoctorJoinForm, self).clean()

    def save(self, commit=True):
        data = self.cleaned_data
        doctor = Doctor.objects.create(member=self.member,
                                       degree=data['degree'],
                                       degree_year=data['degree_year'],
                                       university=data['university'],
                                       clinic_phone=data['clinic_phone'],
                                       clinic_address=data['clinic_address'],
                                       contract=data['contract'])
        return doctor
