import logging

from django.test.utils import override_settings
from rest_framework.test import APITestCase


@override_settings(
    PASSWORD_HASHERS=('django.contrib.auth.hashers.MD5PasswordHasher',),
    DEBUG=False, TEMPLATE_DEBUG=False, LOGGING_CONFIG=None)
class BaseTestCase(APITestCase):
    def __init__(self, *args, **kwargs):
        super(BaseTestCase, self).__init__(*args, **kwargs)
        logging.disable(logging.CRITICAL)
        self.files = []
        self.jwt_token = None

    def _post_teardown(self):
        for file in self.files:
            file.close()
        super(BaseTestCase, self)._post_teardown()
