"""untitled1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic.base import TemplateView

from account.urls import mobile_urlpatterns as account_mobile_url
from advertisement.urls import mobile_urlpatterns as advertisement_mobile_url
from schedule.urls import mobile_urlpatterns as schedule_mobile_url

mobile_urlpatterns = [
    url(r'^', include(account_mobile_url, namespace='account')),
    url(r'^schedule/', include(schedule_mobile_url, namespace='schedule')),
    url(r'^advertisement/', include(advertisement_mobile_url, namespace='advertisement')),
]

urlpatterns = static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) + [
    url(r'^admin/', admin.site.urls),
    url(r'^api/', include(mobile_urlpatterns, namespace='api')),
    url(r'^', include('account.urls', namespace='account')),
    url(r'^schedule/', include('schedule.urls', namespace='schedule')),
    url(r'^advertisement/', include('advertisement.urls', namespace='advertisement')),
    url(r'^$', TemplateView.as_view(
        template_name='back-doctor/home-page.html'), name='home'),
]
