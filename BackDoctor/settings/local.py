from BackDoctor.settings.base import *

__author__ = 'FERIne'

SITE_URL = 'http://localhost:8000'

PROJECT_APPS = [
    'utils',
    'account',
    'schedule',
    'advertisement',
]

INSTALLED_APPS += PROJECT_APPS

STORAGE = 'default'

LOGIN_REDIRECT_URL = '/'
LOGIN_URL = '/login/'
LOGOUT_URL = '/logout/'

ALLOWED_HOSTS = ["*"]

DOCTOR_PROMOTION_LIMIT_PER_MONTH = 100
PROMOTE_SEARCH_LIMIT = 3
ADVERTISE_LIMIT = 3
