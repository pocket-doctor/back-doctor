import dj_database_url

from BackDoctor.settings.local import *  # NOQA

DEBUG = False

ALLOWED_HOSTS = ['pocket-doctor.herokuapp.com']

SITE_URL = 'https://pocket-doctor.herokuapp.com'
SITE_ID = 1

ADMINS = (('Pocket Doctor Admin', 'admin@hebbeh.ir'),)
MANAGERS = ADMINS

DATABASES = {'default': dj_database_url.config()}

STORAGE = 'google'
