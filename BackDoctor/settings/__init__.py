import os

__author__ = 'FERIne'

# Secret Keys
os.environ["ADMIN_PASSWORD"] = os.environ.get("ADMIN_PASSWORD", "12345")
os.environ["SECRET_KEY"] = os.environ.get("SECRET_KEY", "some random key")
os.environ["EMAIL_HOST"] = os.environ.get("EMAIL_HOST", "smtp.gmail.com")
os.environ["EMAIL_ADDRESS"] = os.environ.get("EMAIL_ADDRESS", "hebbeh.logs@gmail.com")
os.environ["EMAIL_ADDRESS_JOIN"] = os.environ.get("EMAIL_ADDRESS_JOIN", "hebbeh.logs@gmail.com")
os.environ["EMAIL_PASSWORD"] = os.environ.get("EMAIL_PASSWORD", "$#ax6B8QaMX$UXCZ$WWow*IqMd3QD0")
os.environ["EMAIL_PORT"] = os.environ.get("EMAIL_PORT", "587")
os.environ["EMAIL_USE_TLS"] = os.environ.get("EMAIL_USE_TLS", "True")

# Primary django setting
if 'DATABASE_URL' in os.environ:
    from BackDoctor.settings.development import *
else:
    from BackDoctor.settings.local import *
